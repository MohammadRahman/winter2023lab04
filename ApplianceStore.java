import java.util.Scanner;
public class ApplianceStore {
	public static void main(String[] args) {
		Scanner reader = new Scanner(System.in);
		Television[] Tv = new Television[4];
		for (int i = 0; i < Tv.length; i++) {
			//Tv[i] = new Television(reader.nextInt(), reader.nextInt(), reader.next());
			System.out.println("Enter the screen size (in inch) for tv " + (i+1) + " : ");
			int screen = reader.nextInt();
			System.out.println("Enter the resolution (480?, 720?, 1080? etc) for tv " + (i+1) + " : ");
			int pixel = reader.nextInt();
			System.out.println("Enter the brand for tv " + (i+1) + " : ");
			String company = reader.next();
			System.out.println("Enter brightness level for tv " + (i+1) + " : ");
			int bright = reader.nextInt();
			Tv[i] = new Television(screen, pixel, company, bright);
		}
		System.out.println("The last TV size (in inch): " + Tv[Tv.length - 1].getSize() + ", the last Tv resolution (in pixel): " + Tv[Tv.length - 1].getResolution() + ", the last Tv brand: " + Tv[Tv.length - 1].getBrand() + ", your last Tv brightness: " + Tv[Tv.length-1].getBrightness());
		System.out.println("Enter the NEW screen size (in inch) for tv 4 : ");
		Tv[3].setSize(reader.nextInt());
		System.out.println("Enter the NEW resolution (480?, 720?, 1080? etc) for tv 4 : ");
		Tv[3].setResolution(reader.nextInt());
		System.out.println("Enter the NEW brand for tv 4 : ");
		Tv[3].setBrand(reader.next());
		System.out.println("Enter NEW brightness level for tv 4 : ");
		Tv[3].setBrightness(reader.nextInt());
		System.out.println("UPDATED: The last TV size (in inch): " + Tv[Tv.length - 1].getSize() + ", the last Tv resolution (in pixel): " + Tv[Tv.length - 1].getResolution() + ", the last Tv brand: " + Tv[Tv.length - 1].getBrand() + ", your last Tv brightness: " + Tv[Tv.length-1].getBrightness());
		Tv[0].mode();
		Tv[0].openApp();
		Tv[1].changeBrightness(25);
	}
}