public class Television {
	private int size;
	public int getSize(){
		return this.size;
	}
	public void setSize(int newSize){
		this.size = newSize;
	}
	
	private int resolution;
	public int getResolution(){
		return this.resolution;
	}
	public void setResolution(int newResolution){
		this.resolution = newResolution;
	}
	
	private String brand;
	public String getBrand(){
		return this.brand;
	}
	public void setBrand(String newBrand){
		this.brand = newBrand;
	}
	
	private int brightness;
	public int getBrightness(){
		return this.brightness;
	}
	public void setBrightness(int newBrightness){
		this.brightness = newBrightness;
	}
	
	
	public void mode() {
		if (size>=75){
			System.out.println("Your TV is in Cinema Mode");
		}
		else if (size<75 && size>=45){
			System.out.println("Your TV is in Sports Mode");
		}
		else {
			System.out.println("Your TV is in Game Mode");
		}
	}
	public void openApp() {
		if (resolution>=1080){
			System.out.println("Opening Netflix...");
		}
		else if (resolution>=720 && resolution<1080) {
			System.out.println("Opening Roku...");
		}
		else if (resolution>=480 && resolution<720) {
			System.out.println("Opening YouTube...");
		}
		else{
			System.out.println("Opening Prime Video...");
		}
		
	}
	public void changeBrightness(int amount){
		if (add(amount)){
		this.brightness = this.brightness + amount;
		System.out.println("Tv's updated brightness: " + this.brightness);
		}
		else {
		this.brightness = this.brightness - amount;	
		System.out.println("Tv's updated brightness: " + this.brightness);
		}
		
	}
	private boolean add(int amount){
		boolean addBrightness = this.brightness<=amount;
		return addBrightness;
	}
	public Television(int aSize, int aResolution, String aBrand, int aBrightness){
		this.size = aSize;
		this.brand = aBrand;
		this.resolution = aResolution;
		this.brightness = aBrightness;
	}
}
	